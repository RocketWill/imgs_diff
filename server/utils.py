#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: willc
"""

import os
import glob
import base64


def check_files_count(folders_list):
    try:
        cnt = None
        for fd in folders_list:
            files_num = len(glob.glob(os.path.join(fd, "*.jpg")))
            if not cnt:
                cnt = files_num
            else:
                if cnt != files_num:
                    return False
        return True
    except:
        return False

def get_files_path(folders_list):
    res = []
    print(folders_list)
    try:
        for fd in folders_list:
            imgs_path = glob.glob(os.path.join(fd, "*.jpg"))
            imgs_path.sort()
            imgs_base64s = [encode_img(img) for img in imgs_path ]
            res.append(imgs_base64s)
        return res
    except Exception as e:
        print(e)
        return res

def encode_img(img):
    img_ext = os.path.splitext(img)[-1]
    if img_ext == '.jpg':
        header = 'data:image/jpeg;base64,'
    elif img_ext == '.png':
        header = 'data:image/png;base64,'
    else:
        raise ValueError('not support format: {}'.format(img_ext))

    with open(img, "rb") as image_file:
        encoded_string = base64.b64encode(image_file.read()).decode('ascii')
    encoded_string = header + encoded_string
    # encoded_string = "data:image/png;base64, iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO9TXL0Y4OHwAAAABJRU5ErkJggg=="
    return encoded_string