from flask import Flask
from flask_restplus import Resource, Api
from flask_cors import CORS
import os, ntpath, glob
from flask_restplus import reqparse
import utils

app = Flask(__name__)                  #  Create a Flask WSGI application
api = Api(app)                         #  Create a Flask-RESTPlus API
CORS(app, resources={r'/*': {'origins': '*'}})

parser = reqparse.RequestParser()
parser.add_argument('folders', action='split')
@api.route('/list-imgs/')
class ListImages(Resource):
    @api.expect(parser)
    def get(self):
        resp = {"code": 0, "msg": "success", "data": []}
        args = parser.parse_args()
        try:
            folders = args['folders']
            is_valid = utils.check_files_count(folders)
            if not is_valid:
                resp = {"code": 1, "msg": "len not match", "data": []}
                return resp, 200

            files_path_list = utils.get_files_path(folders)
            resp['data'] = files_path_list
            return resp, 200
        except:
            resp = {"code": 2, "msg": "error", "data": []}
            return resp, 500

if __name__ == '__main__':
    app.run(debug=True, host="0.0.0.0")                #  Start a development server
