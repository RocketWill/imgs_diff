docker run \
    -it \
    --rm \
    -v ${PWD}/client:/app \
    -v /app/node_modules \
    -p 3001:3000 \
    -e CHOKIDAR_USEPOLLING=true \
    img_diff:0.0.1