
import styles from './viz.css';
import React, { Component } from 'react';
import Viz from '../components/Viz';

class viz extends Component {
    render() {
        const {
            location:{
              query:{ foldersInfo }
            }
          } = this.props;
        return (
            <div>
                <Viz foldersInfo={foldersInfo} />
            </div>
        );
    }
}

export default viz;
