import styles from './index.css';
import { Row, Col } from 'antd';
import InputPath from '../components/InputPath';

export default function() {
  return (
    <div className={styles.normal}>
      <Row>
        <Col span={12} offset={6}>
          <InputPath />
        </Col>
      </Row>
    </div>
  );
}
