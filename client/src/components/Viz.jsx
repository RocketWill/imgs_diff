import React, { Component } from 'react';
import { Row, Col, message, Card, Image, Spin, Space } from 'antd';

const axios = require('axios');
const path = require('path');

class Viz extends Component {
    state = {
        foldersInfo: this.props.foldersInfo,
        filesPath: [],
        loading: true,
        returnCode: -1
    }

    componentDidMount() {
        axios.get('http://192.168.1.122:5000/list-imgs/', {
            params: {
                folders: this.state.foldersInfo + ""
            }
        })
            .then((response) => {
                const data = response.data
                // console.log(data)
                if (data.code == 1) {
                    message.warning('The number of images in each folder should be equal.');
                    this.setState({ returnCode: 1, loading: false })
                }
                else if (data.code == 2) {
                    message.error('There is some error, please try again.');
                    this.setState({ returnCode: 2, loading: false })
                }
                else {
                    this.setState({ filesPath: data.data, returnCode: 0, loading: false })
                }

            })
            .catch(function (error) {
                message.error(error);
            })
    }

    disPlayCards = (oneFolderFiles, numCols) => {
        const cards = []

        oneFolderFiles.map((filePath, idx) => {
            const basename = path.basename(filePath);
            // const relativePath = path.relative(currentPath, '/Users/willc/Desktop/images/a');
            // const reqSvgs = require.context(relativePath, true, /\.jpg$/);
            

            cards.push(
                <Card title={idx} key={`image_${idx}`} style={{ minWidth: `${100 / numCols}-1%` }}>
                    <Image
                        width="100%"
                        src={filePath}
                    />
                </Card>)
        })
        return cards
    }

    getColumns = (num) => {
        const cols = []
        const { filesPath } = this.state
        for (let i = 0; i < num; i++) {
            const span = 24 / num
            cols.push(<Col span={span} key={`col_${i}`}>{this.disPlayCards(filesPath[i], num)}</Col>)
        }
        return cols
    }

    render() {
        const { foldersInfo, filesPath, returnCode, loading } = this.state

        if (returnCode != 0) {
            return (
                <Space size="middle" style={{marginTop: 100}}>
                <Spin size="large" />
            </Space>
            )
        }
        // this.listFilesInDir("/Users/willc/Desktop/images/a")
        else if (filesPath.length > 0) {
            return (
                <>
                    <div style={{marginTop: 30}}></div>
                    <Row>
                        {this.getColumns(foldersInfo.length)}
                    </Row>

                </>
            )
        }
        return <h1>Cannot not get folders info</h1>
    }
}

export default Viz;
