import React, { Component } from 'react'
import { Link } from 'umi'
import { Input, Button, Radio, message } from 'antd'
import { FolderOpenOutlined, PlusOutlined, MinusOutlined } from '@ant-design/icons';
const { TextArea } = Input


class InputPath extends Component {

    state  = {
        foldersInfo: [],
        numOfInputs: 1
    }

    onChange = (e) => {
        console.log(e);
    }

    handleInputeChange = (e) => {
        const value = e.target.value
        const inputId = parseInt(e.target.dataset.inputid)
        const newFoldersInfo = [...this.state.foldersInfo]
        if (newFoldersInfo.length <= inputId) {
            newFoldersInfo.push(value.trim())
        }
        else {
            newFoldersInfo[inputId] = value.trim()
        }

        this.setState({foldersInfo: newFoldersInfo})

    }

    getInputs = (num) => {
        const lst = []
        for (let i=0; i<num; i++) {
            lst.push(<Input size="large" onChange={this.handleInputeChange} key={`input_${i}`} data-inputid={i} placeholder="Enter folder path" prefix={<FolderOpenOutlined />} style={{ marginTop: 10 }} />)
        }
        return lst
    }

    handleAddFolderClick = () => {
        let curNumInputs = this.state.numOfInputs
        let newNum = curNumInputs + 1
        if (newNum > 4) {
            newNum = 4
            message.warning('We only support 4 folers for now.');
        }
        this.setState({numOfInputs: newNum})
        console.log(this.state)
    }

    handleMinusFolderClick = () => {
        let curNumInputs = this.state.numOfInputs
        let newNum = curNumInputs - 1
        const curFoldersInfo = this.state.foldersInfo
        
        if (newNum < 1) {
            newNum = 1
            message.warning('You have to enter at least on folder path.');
        }
        else {
            curFoldersInfo.pop()
        }
        this.setState({numOfInputs: newNum, foldersInfo: curFoldersInfo})
        
    }

    // handleSubmit = () => {
    //     const {foldersInfo} = this.state;
    //     <Link to={{pathname:'/viz', query:{foldersInfo}}}>查看</Link>
    // }

    render() {
        const {numOfInputs,foldersInfo} = this.state
        console.log(numOfInputs)
        return (
            <>
                <Button type="primary" shape="circle" icon={<PlusOutlined />} size="large" onClick={this.handleAddFolderClick}/>
                <Button type="primary" shape="circle" icon={<MinusOutlined />} size="large" onClick={this.handleMinusFolderClick} style={{marginLeft: 20 }}/>
                {this.getInputs(numOfInputs)}
                <br />
                <Button type="primary" shape="round" size="large" style={{marginTop: 20}} ><Link to={{pathname:'/viz', query:{foldersInfo}}}>Submit</Link></Button>
            </>
        );
    }
}

export default InputPath;
