import styles from './index.css';
import { Link } from 'umi';

function BasicLayout(props) {
  return (
    <div className={styles.normal}>
      <Link to="/"><h1 className={styles.title} styles={{color: "#fff"}}>WaImages Diff Online</h1></Link>
      {props.children}
    </div>
  );
}

export default BasicLayout;
